<?php
namespace ariwiraasmara\laravel_repository_service\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;

class ServiceCommand extends Command {
    protected $signature = 'make:service {name}';

    protected $description = 'Generate a service file';

    public function handle() {
        // Logika perintah Anda di sini
        $name = $this->argument('name');
        Artisan::call("make:service {$name}");
        $interfaceOption = $this->option('i');

        // ...

        $serviceStub = $this->getStubContent('Service');
        $interfaceStub = $this->getStubContent('Interface');

        $serviceContent = str_replace('{{name}}', $name, $serviceStub);
        $interfaceContent = str_replace('{{name}}', $name, $interfaceStub);

        $serviceFilePath = app_path('Http/Services/' . $name . 'Service.php');
        $interfaceFilePath = app_path('Interfaces/Services/' . $name . 'ServiceInterface.php');

        \File::put($serviceFilePath, $serviceContent);
        \File::put($interfaceFilePath, $interfaceContent);
    }
}
