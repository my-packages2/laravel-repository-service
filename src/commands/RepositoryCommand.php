<?php
namespace ariwiraasmara\laravel_repository_service\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Artisan;

class RepositoryCommand extends Command {
    protected $signature = 'make:repository {name}';

    protected $description = 'Generate a repository file';

    public function handle() {
        // Logika perintah Anda di sini
        $name = $this->argument('name');
        Artisan::call("make:repository {$name}");
        $interfaceOption = $this->option('i');

        // ...

        $repositoryStub = $this->getStubContent('Repository');
        $interfaceStub = $this->getStubContent('Interface');

        $repositoryContent = str_replace('{{name}}', $name, $repositoryStub);
        $interfaceContent = str_replace('{{name}}', $name, $interfaceStub);

        $repositoryFilePath = app_path('Http/Repositories/' . $name . 'Repository.php');
        $interfaceFilePath = app_path('Interfaces/Repositories/' . $name . 'RepositoryInterface.php');

        \File::put($repositoryFilePath, $repositoryContent);
        \File::put($interfaceFilePath, $interfaceContent);
    }
}
