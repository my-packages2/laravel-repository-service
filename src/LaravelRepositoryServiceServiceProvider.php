<?php
namespace ariwiraasmara\laravel_repository_service;
use Illuminate\Support\ServiceProvider;

class LaravelRepositoryServiceServiceProvider extends ServiceProvider {
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Commands\RepositoryCommand::class,
                Commands\ServiceCommand::class,
            ]);
        }
    }
}
